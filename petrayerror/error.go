package petrayerror

type PetrayError struct {
	ErrCode int64  `json:"code"`
	Message string `json:"message"`
	meta    map[string]string
}

func (pe *PetrayError) Error() string {
	return pe.Message
}

func New(code int64, msg string) *PetrayError {
	return &PetrayError{ErrCode: code, Message: msg, meta: map[string]string{}}
}
