package petrayerror

import "github.com/twitchtv/twirp"

const (
	IncorrectUserIdCode = 1000
	IncorrectUserIdMsg  = "IncorrectUserId"

	EmptyPhoneCode = 1000
	EmptyPhoneMsg  = "Empty Phone"

	IncorrectPasswordCode = 1000
	IncorrectPasswordMsg  = "IncorrectPassword"

	SamePasswordsCode = 1000
	SamePasswordsMsg  = "SamePasswords"

	NotFoundCode = 1000
	NotFoundMsg  = "NotFound"

	UserAlreadyExistCode = 1000
	UserAlreadyExistMsg  = "UserAlreadyExist"

	IncorrectAlbumIdCode = 1000
	IncorrectAlbumIdMsg  = "IncorrectAlbumId"

	EmptyNameCode = 1000
	EmptyNameMsg  = "EmptyName"

	IncorrectTypeCode = 1000
	IncorrectTypeMsg  = "IncorrectType"

	EmptyValueCode = 1000
	EmptyValueMsg  = "EmptyValue"

	IncorrectPhoneCode = 1000
	IncorrectPhoneMsg  = "IncorrectPhone"

	IncorrectGenderCode = 1000
	IncorrectGenderMsg  = "IncorrectGender"

	EmptyEmailCode = 1000
	EmptyEmailMsg  = "EmptyEmail"

	IncorrectLangCode = 1000
	IncorrectLangMsg  = "IncorrectLang"

	EmptyValuesCode = 1000
	EmptyValuesMsg  = "EmptyValues"

	ForbiddenCode = 403
	ForbiddenMsg  = "Forbidden"

	UnauthorizedCode = 401
	UnauthorizedMsg  = "Unauthorized"

	AccessDeniedCode = 1000
	AccessDeniedMsg  = "AccessDenied"

	EmptyIdsCode = 1000
	EmptyIdsMsg  = "EmptyIds"

	IncorrectPhotoIdCode = 1000
	IncorrectPhotoIdMsg  = "IncorrectPhotoId"

	IncorrectCodeCode = 1000
	IncorrectCodeMsg  = "IncorrectCode"

	Code = 1000
	Msg  = ""
)

var (
	IncorrectUserIdErr   = New(IncorrectUserIdCode, IncorrectUserIdMsg)
	EmptyPhoneErr        = New(EmptyPhoneCode, EmptyPhoneMsg)
	IncorrectCodeErr     = New(IncorrectCodeCode, IncorrectCodeMsg)
	IncorrectPasswordErr = New(IncorrectPasswordCode, IncorrectPasswordMsg)
	SamePasswordsErr     = New(SamePasswordsCode, SamePasswordsMsg)
	NotFoundErr          = New(NotFoundCode, NotFoundMsg)
	UserAlreadyExistErr  = New(UserAlreadyExistCode, UserAlreadyExistMsg)
	IncorrectPhoneErr    = New(IncorrectPhoneCode, IncorrectPhoneMsg)
	IncorrectAlbumIdErr  = New(IncorrectAlbumIdCode, IncorrectAlbumIdMsg)
	IncorrectPhotoIdErr  = New(IncorrectPhotoIdCode, IncorrectPhotoIdMsg)
	EmptyIdsErr          = New(EmptyIdsCode, EmptyIdsMsg)
	IncorrectGenderErr   = New(IncorrectGenderCode, IncorrectGenderMsg)
	EmptyNameErr         = New(EmptyNameCode, EmptyNameMsg)
	EmptyEmailErr        = New(EmptyEmailCode, EmptyEmailMsg)
	IncorrectTypeErr     = New(IncorrectTypeCode, IncorrectTypeMsg)
	IncorrectLangErr     = New(IncorrectLangCode, IncorrectLangMsg)
	EmptyValueErr        = New(EmptyValueCode, EmptyValueMsg)
	EmptyValuesErr       = New(EmptyValuesCode, EmptyValuesMsg)
	UnauthorizedErr      = New(UnauthorizedCode, UnauthorizedMsg)
	AccessDeniedErr      = New(AccessDeniedCode, AccessDeniedMsg)
	ForbiddenErr         = New(ForbiddenCode, ForbiddenMsg)
	Err                  = New(Code, Msg)
)

func (pe *PetrayError) Code() twirp.ErrorCode {
	return twirp.ErrorCode(pe.Message)
}
func (pe *PetrayError) Msg() string {
	return pe.Message
}
func (pe *PetrayError) WithMeta(key string, val string) twirp.Error {
	newErr := &PetrayError{
		ErrCode: pe.ErrCode,
		Message: pe.Message,
		meta:    make(map[string]string, len(pe.meta)),
	}
	for k, v := range pe.meta {
		newErr.meta[k] = v
	}
	newErr.meta[key] = val
	return newErr
}
func (pe *PetrayError) Meta(key string) string {
	return pe.meta[key]
}
func (pe *PetrayError) MetaMap() map[string]string {
	return pe.meta
}
