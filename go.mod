module gitlab.com/petray/common

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/json-iterator/go v1.1.10
	github.com/pkg/errors v0.9.1 // indirect
	github.com/twitchtv/twirp v5.12.1+incompatible
)
