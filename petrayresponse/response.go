package petrayresponse

import (
	"github.com/json-iterator/go"
	"github.com/twitchtv/twirp"
	"gitlab.com/petray/common/petrayerror"
	"net/http"
)

type PetrayResponse struct {
	Response interface{}              `json:"response"`
	Error    *petrayerror.PetrayError `json:"error"`
}

func Success(response interface{}) *PetrayResponse {
	return &PetrayResponse{Response: response}
}
func Forbidden() *PetrayResponse {
	return petrayHttpErr(http.StatusForbidden)
}
func petrayHttpErr(code int) *PetrayResponse {
	return &PetrayResponse{Error: petrayerror.New(int64(code), http.StatusText(code))}
}

func Failure(code int64, msg string) *PetrayResponse {
	return &PetrayResponse{Error: petrayerror.New(code, msg)}
}
func FailureErr(err *petrayerror.PetrayError) *PetrayResponse {
	return &PetrayResponse{Error: err}
}
func New(resposne interface{}) *PetrayResponse {
	return &PetrayResponse{Response: resposne}
}
func WriteResponse(w http.ResponseWriter, response interface{}) {
	WriteApiResponse(w, New(response))
}
func WriteApiResponse(w http.ResponseWriter, response *PetrayResponse) {
	w.WriteHeader(http.StatusOK)
	jsoniter.NewEncoder(w).Encode(response)
}
func WriteHttpError(w http.ResponseWriter, code int) {
	w.WriteHeader(code)
	jsoniter.NewEncoder(w).Encode(petrayHttpErr(code))
}
func WriteApiError(w http.ResponseWriter, err *PetrayResponse) {
	w.WriteHeader(http.StatusOK)
	jsoniter.NewEncoder(w).Encode(err)
}
func WriteError(w http.ResponseWriter, err error) {
	if prerr, ok := err.(*petrayerror.PetrayError); ok {
		if prerr.ErrCode == petrayerror.UnauthorizedCode {
			w.WriteHeader(http.StatusUnauthorized)
			jsoniter.NewEncoder(w).Encode(prerr)
			return
		} else if prerr.ErrCode == petrayerror.ForbiddenCode {
			w.WriteHeader(http.StatusForbidden)
			jsoniter.NewEncoder(w).Encode(prerr)
			return
		}
	}
	if twerr, ok := err.(twirp.Error); ok {
		code := twirp.ServerHTTPStatusFromErrorCode(twerr.Code())
		w.WriteHeader(code)
		jsoniter.NewEncoder(w).Encode(petrayHttpErr(code))
		return
	}
	w.WriteHeader(http.StatusOK)
	jsoniter.NewEncoder(w).Encode(petrayHttpErr(http.StatusInternalServerError))
}
